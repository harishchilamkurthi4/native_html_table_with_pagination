# TableApp

TableApp  with pagination, without any third party components

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## node_modules
Download all node packages using `npm i`

## Start Project
Run `npm start` to start the project

## Custom data
Replace sample data with custom data, 
`native_html_table_with_pagination/src/assets/data/sample_data.json`

## Build

Run `npm build` to build the project. The build artifacts will be stored in the `dist/` directory. 

## opening index.html, doesnt work 
Standard security reason is that all modern browsers block cross origin requests.

Install chrome plugin and enable it: `chrome plugin to enable cors`

If this does not work, then you should try to run your project on a http-server. Have a look this answer: http-server to run local projects.
## Reason
Cross origin requests are only supported for protocol schemes: http, data, chrome, chrome-extension, https

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Checking lint

Run `ng lint`.